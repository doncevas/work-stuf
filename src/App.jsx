import React, { useState, useEffect } from 'react'
import Filters from './views/Filters'
import PlayerCards from './views/PlayerCards'
// import axios from 'axios'
import { css } from '@emotion/core'

import './styles/App.css'

const db = {
  data: [
    {
      id: 1,
      contentType: ['webinar', 'animation'],
      title: '1 to 1 Coaching: Sprinting Technique',
      author: {
        name: 'James Parr',
        image: {
          src: 'https://cdn.thecoachingmanual.com/front_end_technical_test_assets/avatar_1.jpg',
          alt: 'Portrait of a man smiling.',
        },
      },
      image: {
        src: 'https://smaller-pictures.appspot.com/images/dreamstime_xxl_65780868_small.jpg',
        alt: 'Two footballers jostling for the ball with a coach watching in the background.',
      },
    },
    {
      id: 2,
      contentType: ['animation'],
      title: '1 to 1 Coaching: Sprinting Technique',
      author: {
        name: 'James Parr',
        image: {
          src: 'https://cdn.thecoachingmanual.com/front_end_technical_test_assets/avatar_1.jpg',
          alt: 'Portrait of a man smiling.',
        },
      },
      image: {
        src: 'https://smaller-pictures.appspot.com/images/dreamstime_xxl_65780868_small.jpg',
        alt: 'Two footballers jostling for the ball with a coach watching in the background.',
      },
    },
    {
      id: 3,
      contentType: ['infographic'],
      title: '1 to 1 Coaching: Sprinting Technique',
      author: {
        name: 'James Parr',
        image: {
          src: 'https://cdn.thecoachingmanual.com/front_end_technical_test_assets/avatar_1.jpg',
          alt: 'Portrait of a man smiling.',
        },
      },
      image: {
        src: 'https://i.pinimg.com/originals/af/bc/6d/afbc6d507071340bc8a2e0ce0d600a1b.jpg',
        alt: 'Two footballers jostling for the ball with a coach watching in the background.',
      },
    },
    {
      id: 4,
      contentType: ['video'],
      title: '1 to 1 Coaching: Sprinting Technique',
      author: {
        name: 'James Parr',
        image: {
          src: 'https://cdn.thecoachingmanual.com/front_end_technical_test_assets/avatar_1.jpg',
          alt: 'Portrait of a man smiling.',
        },
      },
      image: {
        src: 'https://i.pinimg.com/originals/af/bc/6d/afbc6d507071340bc8a2e0ce0d600a1b.jpg',
        alt: 'Two footballers jostling for the ball with a coach watching in the background.',
      },
    },
  ],
}

const topMenuFilter = css`
  display: flex;
  justify-content: flex-end;
  .filter {
    position: relative;
    display: flex;
    align-items: center;
    span {
      position: relative;
      top: 1px;
      font-style: normal;
      font-weight: 600;
      font-size: 10px;
      text-align: center;
      letter-spacing: 0.05em;
      text-transform: uppercase;
    }
  }
`

export default function App() {
  const [state, setState] = useState(db.data)
  const [filter, setFilter] = useState([])

  useEffect(() => {
    const allFilters = db.data.map((e) => e.contentType)
    var merged = [].concat.apply([], allFilters)
    const uniqueFilter = merged.filter((v, i, a) => a.indexOf(v) === i)
    setFilter(uniqueFilter);
  }, [state])

  const filterDate = (selectedFilters) => {
    console.log(selectedFilters);
    const filteredData = db.data.filter((item) =>
      item.contentType.some((filter) => !!selectedFilters.includes(filter)),
    )
    
    setState(filteredData);
  }

  return (
    <>
      <div css={topMenuFilter}>
        <div className="filter">
          <span>Filters</span>
          <Filters filters={filter} onChange={(e) => filterDate(e)}/>
        </div>
      </div>

      <PlayerCards players={state} />
    </>
  )
}
