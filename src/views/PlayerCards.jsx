import React from 'react'
import RoundButton from '../components/RoundButton'
import Tag from '../components/Tag'
import Play from '../components/svg/play'
import Movie from '../components/svg/movie'
import { css } from '@emotion/core'

const mainBox = css`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  flex-direction: row;
  align-items: center;
  align-content: center;
  .card {
    position: relative;
    margin: 4% 0;
    width: 340px;
    height: 544px;
    border-radius: var(--radius);
    background-color: var(--white);
    &-img {
      border-radius: var(--radius) var(--radius) 0 0;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      width: 340px;
      height: 340px;
    }
    &-description {
      padding: 0 30px;
      .tag {
        position: absolute;
        top: 370px;
      }
      .title {
        position: absolute;
        top: 406px;
        font-size: 22px;
        font-family: Montserrat;
        font-style: normal;
        font-weight: bold;
        color: #222222;
      }
      .author {
        position: absolute;
        top: 484px;
        width: 264px;
        height: 30px;
        display: flex;
        &-image {
          display: inline-block;
          border-radius: 50%;
        }
        &-name {
          padding-left: 6px;
          font-style: normal;
          font-weight: 500;
          font-size: 12px;
          line-height: 15px;
          color: #555555;
          display: flex;
          align-items: center;
        }
      }
    }
  }
`

export default function Cards({ players }) {
  // const [data] = useState(players)
  return (
    <div css={mainBox}>
      {players.map(({ id, contentType, title, author, image }) => {
        return (
          <div key={id} className="card">
            <div
              className="card-img"
              style={{ backgroundImage: `url(${image.src})` }}
              alt={image.alt}
            >
              {contentType.toString() === 'video' ? <RoundButton buttonType={<Play />} /> : null}

              {contentType.toString() === 'animation' ? (
                <RoundButton buttonType={<Movie />} />
              ) : null}
            </div>

            <div className="card-description">
              <div className="tag">
                <Tag contentType={contentType} />
              </div>

              <div className="title">{title}</div>

              <div className="author">
                <img
                  className="author-image"
                  src={author.image.src}
                  alt={author.image.alt}
                  height="30"
                  width="30"
                />

                <div className="author-name">{author.name}</div>
              </div>
            </div>
          </div>
        )
      })}
    </div>
  )
}
