import React, { useState, useEffect } from 'react'
import { css } from '@emotion/core'
import { video, animation, infographic, webinar, white, black } from '../styles/App.css'

const filtesCss = css`
  padding: 7.5px 12px;
  border-radius: 50px;
  cursor: pointer;
  display: flex;
  flex-direction: row;
  margin: 18px;
  text-transform: uppercase;
  border: 0px none;
  .sircle {
    pointer-events: none;
    position: relative;
    width: 15px;
    height: 15px;
    border-radius: 50%;
  }
  .content-type {
    pointer-events: none;
    padding-top: 2px;
    padding-left: 6px;
    font-style: normal;
    font-weight: 600;
    font-size: 10px;
    line-height: 12px;
    text-align: center;
    letter-spacing: 0.05em;
    text-transform: uppercase;
  }
`

export default function Filters({ filters, onChange }) {
  const [state, setState] = useState([])
  // TODO: remove this i think it useful
  // const test = () => {
  //   for (let index = 1; index < filters.length; index++) {
  //     const string = {}
  //     string.id = index
  //     string.make = filters[index]
  //     string.style = ''
  //     console.log(string)
  //   }
  // }
  // test()
  const colorPicker = (contentType) => {
    if (contentType.toString() === 'webinar') return { backgroundColor: webinar, color: black }
    if (contentType.toString() === 'infographic')
      return { backgroundColor: infographic, color: white }
    if (contentType.toString() === 'animation') return { backgroundColor: animation, color: white }
    if (contentType.toString() === 'video') return { backgroundColor: video, color: white }
  }

  const toggleButton = (selectedFilter, contentType) => {
    let currentFilters = [];
    const currentStyleColor = colorPicker(contentType)
    if (selectedFilter.target.querySelector('div').style.backgroundColor === white) {
      selectedFilter.target.querySelector('div').style.backgroundColor = currentStyleColor.backgroundColor
      selectedFilter.target.style.backgroundColor = ''
      selectedFilter.target.style.color = black
      const updatedState = state.filter((item) => item !== contentType)
      currentFilters = updatedState;
      setState(updatedState);
    } else {
      selectedFilter.target.style.backgroundColor = currentStyleColor.backgroundColor
      selectedFilter.target.style.color = currentStyleColor.color
      selectedFilter.target.querySelector('div').style.backgroundColor = white
      setState([...state, contentType ])
      currentFilters = [...state, contentType]
    }

    onChange(currentFilters);
  }

  return (
    <div style={{ display: 'flex', flexDirection: 'row' }}>
      {filters.map((contentType, index) => (
        <button
          css={filtesCss}
          key={index}
          id={index}
          name={contentType.toString()}
          onClick={(e) => toggleButton(e, contentType)}
        >
          <div className="sircle" name="test" style={colorPicker(contentType)} />
          <div className="content-type">{contentType}</div>
        </button>
      ))}
    </div>
  )
}
