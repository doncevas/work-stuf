import React from 'react'
import { css } from '@emotion/core'
import { video, animation, infographic, webinar, white, black } from '../styles/App.css'

const tag = css`
  padding: 6px 12px;
  float: left;
  margin: 0 5px 0 0;
  border-radius: 1000px;
  text-align: center;
  div {
    position: relative;
    text-align: center;
    font-style: normal;
    font-weight: bold;
    font-size: 10px;
    letter-spacing: 0.05em;
    text-transform: uppercase;
  }
`

export default function Tag({ contentType }) {
  return (
    <>
      {contentType.map((el, index) => {
        const colorPicker = () => {
          if (el === 'webinar') return { backgroundColor: webinar, color: black }
          if (el === 'infographic') return { backgroundColor: infographic, color: white }
          if (el === 'animation') return { backgroundColor: animation, color: white }
          if (el === 'video') return { backgroundColor: video, color: white }
        }

        return (
          <div css={tag} key={index} style={colorPicker()}>
            <div>{el}</div>
          </div>
        )
      })}
    </>
  )
}
